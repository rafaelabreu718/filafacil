﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.Models
{
    public class Supermercado
    {
        public int id { get; set; }
        public int nome { get; set; }
        public string endereco { get; set; }
        public int cep { get; set; }
        public int contato { get; set; }
        public int caixaId { get; set; }


        public List<Caixa> ListaCaixas { get; set; }

        
    }
}