﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.Models
{
    public class Fila
    {
        public int id { get; set; }
        public int usuario { get; set; }
        public int caixaID { get; set; }
        public List<Usuario> ListaUsuarios { get; set; }
        public List<Caixa> ListaCaixas { get; set; }


        
    }
}
