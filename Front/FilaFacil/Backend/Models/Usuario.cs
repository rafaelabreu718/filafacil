﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Backend.Models
{
    public class Usuario
    {
        public int id { get; set; }
        public string nome { get; set; }
        public string sobrenome { get; set; }
        public int cpf { get; set; }
        public string email { get; set; }
        public int dataDeNacimento { get; set; }

    }
}
